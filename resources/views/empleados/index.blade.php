index empleado

<table class="table table-light">

<thead class="thead-light">
<tr>
<th>#</th>
<th>Foto</th>
<th>Rut</th>
<th>Nombre</th>
<th>Apellido</th>
<th>Fecha</th>
<th>Cargo</th>
<th>Acciones</th>
</tr>
</thead>

<tbody>
@foreach($empleados as $empleado)
<tr>
    <td>{{$loop->iteration}}</td>
    <td>
    
    <img src="{{ asset('storage').'/'.$empleado->Foto}}" alt="" width="200">
    
 
    </td>
    <td>{{$empleado->Rut}}</td>
    <td>{{$empleado->Nombre}}</td>
    <td>{{$empleado->Apellido}}</td>
    <td>{{$empleado->Fecha}}</td>
    <td>{{$empleado->Cargo}}</td>
    <td>

    <a href="{{ url('/empleados/'.$empleado->id.'/edit') }}">
    Editar
    </a>




    <form  method="post" action="{{ url('/empleados/'.$empleado->id) }}">
    {{csrf_field() }}
    {{ method_field('DELETE') }}
    <button type="submit" onclick="return confirm('¿borrar?');">Borrar</button>
    </form>
    </td>
</tr>
@endforeach
</tbody>
</table>