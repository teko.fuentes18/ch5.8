<form action="{{ url('/empleados/'. $empleado->id) }}" method="post" enctype="multipart/form-data">
 {{ csrf_field() }}

 {{ method_field('PATCH') }}

<label for="Rut">{{'Rut'}}</label>
<input type="text" name="Rut" id="Rut"  value="{{ $empleado->Rut}}">
<br/>

<label for="Nombre">{{'Nombre'}}</label>
<input type="text" name="Nombre" id="Nombre" value="{{ $empleado->Nombre}}">

<br/>
<label for="Apellido">{{'Apellido'}}</label>
<input type="text" name="Apellido" id="Apellido"  value="{{ $empleado->Apellido}}">
<br/>

<label for="Fecha">{{'Fecha'}}</label>
<input type="date" name="Fecha" id="Fecha" value="{{ $empleado->Fecha}}">
<br/>
<label for="Cargo">{{'Cargo'}}</label>
<input type="text" name="Cargo" id="Cargo"  value="{{ $empleado->Cargo}}">

<br/>
<label for="Foto">{{'Foto'}}</label>

<img src="{{ asset('storage').'/'.$empleado->Foto}}" alt="" width="200">
<br/>
<input type="file" name="Foto" id="Foto" value="">



<br/>

<input type="submit" value="editar">
</form>